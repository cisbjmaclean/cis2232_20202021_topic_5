package cis.bo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test the Sundae class
 * @author bjm
 * @since 20201109
 */
public class SundaeJUnitTest {
    
    public SundaeJUnitTest() {
        setUpClass();
        
    }
    
    @BeforeAll
    public static void setUpClass() {
        System.out.println("setting up the class.");
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

     @Test
     public void testCalculateCostSmall() {
         Sundae sundae = new Sundae();
         sundae.setSize(1);
         double cost = sundae.calculateCost();
         Assertions.assertEquals(2.0, cost);     
     }
     
     /**
     * Size = Large
     * costs = 3.25
     * 
     * @since 20201116
     * @author BJM
     */
     @Test
     public void testCalculateCostLarge() {
         Sundae sundae = new Sundae();
         sundae.setSize(2);
         double cost = sundae.calculateCost();
         Assertions.assertEquals(3.25, cost);     
     }

     @Test
     public void testCalculateCostSmallSmarties(){
         Sundae sundae = new Sundae();
         sundae.setSize(1);
         sundae.setSmarties(true);
         double cost = sundae.calculateCost();
         Assertions.assertEquals(2.5, cost);     
     }
     
     @Test
     public void testCalculateCostLargeSmarties(){
         Sundae sundae = new Sundae();
         sundae.setSize(2);
         sundae.setSmarties(true);
         double cost = sundae.calculateCost();
         Assertions.assertEquals(4, cost);     
         
         
         Assertions.
         
     }
     
}

package cis.bo;

/**
 * Represents a dairy bar sundae
 *
 * @author bjm
 * @since 9-Nov-2020
 */
public class Sundae {

    private int size;
    private boolean smarties, hotFudge, peanuts;

    public static final double BASE_COST_SMALL = 2;
    public static final double BASE_COST_LARGE = 3.25;

    public static final double COST_SMALL_TOPPING = 0.5;
    public static final double COST_LARGE_TOPPING = 0.75;

    public static final int SIZE_SMALL = 1;
    public static final int SIZE_LARGE = 2;

    /**
     * Calculate the cost of a Sundae
     *
     * @since 20201109
     * @author BJM
     */
    public double calculateCost() {

        double cost = 0;
        double costTopping = 0;
        
        switch (size) {
            case SIZE_SMALL:
                cost = BASE_COST_SMALL;
                costTopping = COST_SMALL_TOPPING;
                break;
            case SIZE_LARGE:
                cost = BASE_COST_LARGE;
                costTopping = COST_LARGE_TOPPING;
                break;
            default:
        }

        if (smarties) {
            cost += costTopping;
        }

        return cost;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isSmarties() {
        return smarties;
    }

    public void setSmarties(boolean smarties) {
        this.smarties = smarties;
    }

    public boolean isHotFudge() {
        return hotFudge;
    }

    public void setHotFudge(boolean hotFudge) {
        this.hotFudge = hotFudge;
    }

    public boolean isPeanuts() {
        return peanuts;
    }

    public void setPeanuts(boolean peanuts) {
        this.peanuts = peanuts;
    }

}

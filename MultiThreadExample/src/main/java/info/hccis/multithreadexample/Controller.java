package info.hccis.multithreadexample;

import info.hccis.multithreadexample.threads.Count1Thread;
import info.hccis.multithreadexample.threads.Count2Thread;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controls the overall flow of the program.
 *
 * @author bjmac
 * @since 30-Nov-2020
 */
public class Controller {

    public static final long MAX = 1_000_000_000;
    
    private static long thread2Start, thread2Stop, thread2Sum;

    public static long getThread2Start() {
        return thread2Start;
    }

    public static long getThread2Stop() {
        return thread2Stop;
    }

    public static void setThread2Sum(long thread2Sum) {
        Controller.thread2Sum = thread2Sum;
    }
    
    
    
    
    public static void main(String[] args) {

                    Date date = new Date();
            long start = date.getTime();
            
            long expectedValue = MAX*(MAX+1)/2;

            long countFirst = 0;
            for (int i = 0; i <= MAX; i++) {
            countFirst += i;
        }
            
            
            
            Date endDate = new Date();
            long end = endDate.getTime();
            long duration = end-start;
            
            System.out.println("Count="+countFirst+" it took: "+duration+" milliseconds");
            System.out.println("expected="+expectedValue);

        
        
        
        
        
        
        
        
        
        
        try {
            date = new Date();
            start = date.getTime();
            
            expectedValue = MAX*(MAX+1)/2;
            
            
            Count1Thread thread1 = new Count1Thread();
            thread1.setStartNumber(1);
            thread1.setStopNumber(MAX/2);
            thread1.start();
            
            System.out.println("Have started thread 1 moving on....");
            
            thread2Start = MAX/2+1;
            thread2Stop = MAX;
            
            Thread thread2 = new Thread(new Count2Thread());
            thread2.start();
            
            thread1.join();
            thread2.join();
            
            long sum = thread1.getSum()+thread2Sum;
            
             endDate = new Date();
             end = endDate.getTime();
            
            duration = end-start;
            
            System.out.println("Count="+sum+" it took: "+duration+" milliseconds");
            System.out.println("expected="+expectedValue);
            
            System.out.println("finished main thread");
        } catch (InterruptedException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

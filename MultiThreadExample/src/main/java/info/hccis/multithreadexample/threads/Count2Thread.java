package info.hccis.multithreadexample.threads;

import info.hccis.multithreadexample.Controller;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a new thread which implements the Runnable interface
 *
 * @author bjmac
 * @since 30-Nov-2020
 */
public class Count2Thread implements Runnable {


    
    
    @Override
    public void run() {
        long sum = 0;
        for (int i = (int) Controller.getThread2Start(); i <= Controller.getThread2Stop(); i++) {
            sum += i;
        }
        Controller.setThread2Sum(sum);
    }

}

package info.hccis.multithreadexample.threads;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a 
 * @author bjmac
 * @since 30-Nov-2020
 */
public class Count1Thread extends Thread {

    private long startNumber, stopNumber;
    private long sum;

    public long getSum() {
        return sum;
    }

    public long getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(long startNumber) {
        this.startNumber = startNumber;
    }

    public long getStopNumber() {
        return stopNumber;
    }

    public void setStopNumber(long stopNumber) {
        this.stopNumber = stopNumber;
    }

    @Override
    public void run() {
        for (int i = (int) startNumber; i <= stopNumber; i++) {
            sum += i;
        }
    }

    
}

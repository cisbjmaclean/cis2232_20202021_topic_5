package cis.bo;

import cis.bo.TireJob;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author bjmac
 */
public class TireJobJUnitTest {

    public TireJobJUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testsetDiscountRateTires4() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(4);
        tireJob.setBaseCost();
        tireJob.setDiscountRateTireQuantity();
        double discount = tireJob.getDiscountRateTireQuantity();
        assertEquals(0.05, discount, 0.0001);
    }

    @Test
    public void testsetDiscountRateTires3() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setDiscountRateTireQuantity();
        double discount = tireJob.getDiscountRateTireQuantity();
        assertEquals(0.05, discount, 0.0001);
    }

    @Test
    public void testsetDiscountRateTires2() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(2);
        tireJob.setBaseCost();
        tireJob.setDiscountRateTireQuantity();
        double discount = tireJob.getDiscountRateTireQuantity();
        assertEquals(0.0, discount, 0.0001);
    }

    @Test
    public void testsetPremiumRateVehicleType1() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(1);
        tireJob.setPremiumRateVehicleType();
        double premium = tireJob.getPremiumRateForVehicleType();
        assertEquals(0.0, premium, 0.0001);
    }

    @Test
    public void testsetPremiumRateVehicleType2() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(2);
        tireJob.setPremiumRateVehicleType();
        double premium = tireJob.getPremiumRateForVehicleType();
        assertEquals(0.2, premium, 0.0001);
    }

    @Test
    public void testsetPremiumRateVehicleType3() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(3);
        tireJob.setPremiumRateVehicleType();
        double premium = tireJob.getPremiumRateForVehicleType();
        assertEquals(0.3, premium, 0.0001);
    }

    @Test
    public void testsetDiscountRateCustomerType999() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(999);
        tireJob.setDiscountRateCustomerType();
        double result = tireJob.getDiscountRateCustomerType();
        assertEquals(0.0, result, 0.0001);
    }

    @Test
    public void testsetDiscountRateCustomerType1000() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(1000);
        tireJob.setDiscountRateCustomerType();
        double result = tireJob.getDiscountRateCustomerType();
        assertEquals(0.10, result, 0.0001);
    }

    @Test
    public void testsetDiscountRateCustomerType1999() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(1999);
        tireJob.setDiscountRateCustomerType();
        double result = tireJob.getDiscountRateCustomerType();
        assertEquals(0.10, result, 0.0001);
    }

    @Test
    public void testsetDiscountRateCustomerType2000() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(2000);
        tireJob.setDiscountRateCustomerType();
        double result = tireJob.getDiscountRateCustomerType();
        assertEquals(0.20, result, 0.0001);
    }

    @Test
    public void testsetDiscountRateCustomerType2999() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(2999);
        tireJob.setDiscountRateCustomerType();
        double result = tireJob.getDiscountRateCustomerType();
        assertEquals(0.20, result, 0.0001);
    }

    @Test
    public void testsetDiscountRateCustomerType3000() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(3000);
        tireJob.setDiscountRateCustomerType();
        double result = tireJob.getDiscountRateCustomerType();
        assertEquals(0.0, result, 0.0001);
    }

    @Test
    public void testSpecialOrderTruck4() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(4);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(2);
        tireJob.setDiscountRateCustomerType();
        boolean result = tireJob.determineIfIsSpecialJob();
        assertTrue(result);
    }

    @Test
    public void testSpecialOrderTruck3() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(2);
        tireJob.setDiscountRateCustomerType();
        boolean result = tireJob.determineIfIsSpecialJob();
        assertTrue(!result);
    }

    @Test
    public void testSpecialOrderCar4() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(4);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(1);
        tireJob.setDiscountRateCustomerType();
        boolean result = tireJob.determineIfIsSpecialJob();
        assertTrue(!result);
    }

    @Test
    public void testSpecialOrderVan4() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(4);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(3);
        tireJob.setDiscountRateCustomerType();
        boolean result = tireJob.determineIfIsSpecialJob();
        assertTrue(result);
    }

    @Test
    public void testSpecialOrderVan3() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setTypeOfVehicle(3);
        tireJob.setDiscountRateCustomerType();
        boolean result = tireJob.determineIfIsSpecialJob();
        assertTrue(!result);
    }

    @Test
    public void testsetPremiumRatePrimeTime1015() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(3000);
        tireJob.setMonthOfJob(10);
        tireJob.setDayOfJob(15);
        double result = tireJob.getPremiumRatePrimeTime();
        assertEquals(0.0, result, 0.0001);
    }

    @Test
    public void testsetPremiumRatePrimeTime1201() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(3000);
        tireJob.setMonthOfJob(12);
        tireJob.setDayOfJob(01);
        double result = tireJob.getPremiumRatePrimeTime();
        assertEquals(0.0, result, 0.0001);
    }

    @Test
    public void testsetPremiumRatePrimeTime_1115() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(3000);
        tireJob.setMonthOfJob(11);
        tireJob.setDayOfJob(15);
        tireJob.setPremiumRatePrimeTime();
        double result = tireJob.getPremiumRatePrimeTime();
        assertEquals(0.25, result, 0.0001);
    }

    @Test
    public void testsetPremiumRatePrimeTime_1101() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setCustomerNumber(3000);
        tireJob.setMonthOfJob(11);
        tireJob.setDayOfJob(1);
        tireJob.setPremiumRatePrimeTime();
        double result = tireJob.getPremiumRatePrimeTime();
        assertEquals(0.25, result, 0.0001);
    }

    @Test
    public void testsetSensorYes() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setWithSensors(true);
        tireJob.setPremiumRateTireSensor();
        double result = tireJob.getPremiumRateHasSensors();
        assertEquals(0.1, result, "Discount not right for with sensors");
    }

    @Test
    public void testsetSensorNo() {
        TireJob tireJob = new TireJob();
        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setBaseCost();
        tireJob.setWithSensors(false);
        tireJob.setPremiumRateTireSensor();
        double result = tireJob.getPremiumRateHasSensors();
        assertEquals(0.0, result, "Discount not right for without sensors");
    }

}
